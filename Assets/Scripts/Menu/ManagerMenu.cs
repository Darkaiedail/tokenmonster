﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ManagerMenu : MonoBehaviour {

	public void modeHistoryScene(){
		DontDestroyOnLoad(GameObject.Find("Manager"));
		SceneManager.LoadScene(2);
	}

	public void modeCharacters(){
		DontDestroyOnLoad(GameObject.Find("Manager"));
		SceneManager.LoadScene(1);
	}

	public void inventory(){
		DontDestroyOnLoad(GameObject.Find("Manager"));
		SceneManager.LoadScene(4);
	}
}

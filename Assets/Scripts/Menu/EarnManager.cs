﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class EarnManager: MonoBehaviour {

	public int coins;

	public int expRayMimi;
	public int expFireMimi;
	public int expPlantMimi;
	public int expWaterMimi;

	public int[] level = new int[5];
	public int[] life = new int[5];
	public int[] damage = new int[5];

	public int canLevelUpRayMimi;
	public int canLevelUpFireMimi;
	public int canLevelUpPlantMimi;
	public int canLevelUpWaterMimi;

	private bool canPayCoinRayMimi;
	private bool canPayCoinFireMimi;
	private bool canPayCoinPlantMimi;
	private bool canPayCoinWaterMimi;

	public int nextLevelExpRayMimi = 400;
	public int nextLevelExpFireMimi = 400;
	public int nextLevelExpPlantMimi = 400;
	public int nextLevelExpWaterMimi = 400;

	public int nextLevelCoinRayMimi = 100;
	public int nextLevelCoinFireMimi = 100;
	public int nextLevelCoinPlantMimi = 100;
	public int nextLevelCoinWaterMimi = 100;

	public string sceneLevel;
	public int nextLevel;

	public int screws;
	public int nuts;

	void Awake(){
		gameObject.GetComponent<SaveManager>().Load();
	}

	void Update(){
		rayMimiStats();
		fireMimiStats();
		plantMimiStats();
		waterMimiStats();
	}
		
	void rayMimiStats(){
		if(expRayMimi < 400){
			level[0] = 1;
			life[0] = 900;
			damage[0] = 36;
		}
		if(expRayMimi >= 400 && coins >= 100){
			if(canLevelUpRayMimi == 2){
				level[0] = 2;
				life[0] = 950;
				damage[0] = 40;

				if(canPayCoinRayMimi){
					coins -= 100;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 800;
				nextLevelCoinRayMimi = 200;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 800 && coins >= 200){
			if(canLevelUpRayMimi == 3){
				level[0] = 3;
				life[0] = 1000;
				damage[0] = 44;

				if(canPayCoinRayMimi){
					coins -= 200;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 1400;
				nextLevelCoinRayMimi = 300;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 1400 && coins >= 300){
			if(canLevelUpRayMimi == 4){
				level[0] = 4;
				life[0] = 1050;
				damage[0] = 48;

				if(canPayCoinRayMimi){
					coins -= 300;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 2200;
				nextLevelCoinRayMimi = 350;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 2200 && coins >= 350){
			if(canLevelUpRayMimi == 5){
				level[0] = 5;
				life[0] = 1100;
				damage[0] = 52;

				if(canPayCoinRayMimi){
					coins -= 350;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 3200;
				nextLevelCoinRayMimi = 400;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 3200 && coins >= 400){
			if(canLevelUpRayMimi == 6){
				level[0] = 6;
				life[0] = 1150;
				damage[0] = 56;

				if(canPayCoinRayMimi){
					coins -= 400;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 4600;
				nextLevelCoinRayMimi = 450;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 4600 && coins >= 450){
			if(canLevelUpRayMimi == 7){
				level[0] = 7;
				life[0] = 1200;
				damage[0] = 60;

				if(canPayCoinRayMimi){
					coins -= 450;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 7000;
				nextLevelCoinRayMimi = 500;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 7000 && coins >= 500){
			if(canLevelUpRayMimi == 8){
				level[0] = 8;
				life[0] = 1250;
				damage[0] = 64;

				if(canPayCoinRayMimi){
					coins -= 500;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 9800;
				nextLevelCoinRayMimi = 550;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 9800 && coins >= 550){
			if(canLevelUpRayMimi == 9){
				level[0] = 9;
				life[0] = 1300;
				damage[0] = 68;

				if(canPayCoinRayMimi){
					coins -= 550;
					canPayCoinRayMimi = false;
				}

				nextLevelExpRayMimi = 13200;
				nextLevelCoinRayMimi = 600;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expRayMimi >= 13200 && coins >= 600){
			if(canLevelUpRayMimi == 10){
				level[0] = 10;
				life[0] = 1350;
				damage[0] = 72;

				if(canPayCoinRayMimi){
					coins -= 600;
					canPayCoinRayMimi = false;
				}
			}
		}
	}

	void fireMimiStats(){
		if(expFireMimi < 400){
			level[1] = 1;
			life[1] = 1000;
			damage[1] = 28;
		}
		if(expFireMimi >= 400 && coins >= 100){
			if(canLevelUpFireMimi == 2){
				level[1] = 2;
				life[1] = 1050;
				damage[1] = 32;

				if(canPayCoinFireMimi){
					coins -= 100;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 800;
				nextLevelCoinFireMimi = 200;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 800 && coins >= 200){
			if(canLevelUpFireMimi == 3){
				level[1] = 3;
				life[1] = 1100;
				damage[1] = 36;

				if(canPayCoinFireMimi){
					coins -= 200;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 1400;
				nextLevelCoinFireMimi = 300;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 1400 && coins >= 300){
			if(canLevelUpFireMimi == 4){
				level[1] = 4;
				life[1] = 1150;
				damage[1] = 40;	

				if(canPayCoinFireMimi){
					coins -= 300;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 2200;
				nextLevelCoinFireMimi = 350;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 2200 && coins >= 350){
			if(canLevelUpFireMimi == 5){
				level[1] = 5;
				life[1] = 1200;
				damage[1] = 44;

				if(canPayCoinFireMimi){
					coins -= 350;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 3200;
				nextLevelCoinFireMimi = 400;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 3200 && coins >= 400){
			if(canLevelUpFireMimi == 6){
				level[1] = 6;
				life[1] = 1250;
				damage[1] = 48;

				if(canPayCoinFireMimi){
					coins -= 400;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 4600;
				nextLevelCoinFireMimi = 450;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 4600 && coins >= 450){
			if(canLevelUpFireMimi == 7){
				level[1] = 7;
				life[1] = 1300;
				damage[1] = 52;

				if(canPayCoinFireMimi){
					coins -= 450;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 7000;
				nextLevelCoinFireMimi = 500;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 7000 && coins >= 500){
			if(canLevelUpFireMimi == 8){
				level[1] = 8;
				life[1] = 1350;
				damage[1] = 56;

				if(canPayCoinFireMimi){
					coins -= 500;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 9800;
				nextLevelCoinFireMimi = 550;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 9800 && coins >= 550){
			if(canLevelUpFireMimi == 9){
				level[1] = 9;
				life[1] = 1400;
				damage[1] = 60;

				if(canPayCoinFireMimi){
					coins -= 550;
					canPayCoinFireMimi = false;
				}

				nextLevelExpFireMimi = 13200;
				nextLevelCoinFireMimi = 600;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expFireMimi >= 13200 && coins >= 600){
			if(canLevelUpFireMimi == 10){
				level[1] = 10;
				life[1] = 1450;
				damage[1] = 64;

				if(canPayCoinFireMimi){
					coins -= 600;
					canPayCoinFireMimi = false;
				}
			}
		}
	}

	void plantMimiStats(){
		if(expPlantMimi < 400){
			level[2] = 1;
			life[2] = 700;
			damage[2] = 40;
		}
		if(expPlantMimi >= 400 && coins >= 100){
			if(canLevelUpPlantMimi == 2){
				level[2] = 2;
				life[2] = 750;
				damage[2] = 44;

				if(canPayCoinPlantMimi){
					coins -= 100;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 800;
				nextLevelCoinPlantMimi = 200;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 800 && coins >= 200){
			if(canLevelUpPlantMimi == 3){
				level[2] = 3;
				life[2] = 800;
				damage[2] = 48;

				if(canPayCoinPlantMimi){
					coins -= 200;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 1400;
				nextLevelCoinPlantMimi = 300;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 1400 && coins >= 300){
			if(canLevelUpPlantMimi == 4){
				level[2] = 4;
				life[2] = 850;
				damage[2] = 52;

				if(canPayCoinPlantMimi){
					coins -= 300;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 2200;
				nextLevelCoinPlantMimi = 350;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 2200 && coins >= 350){
			if(canLevelUpPlantMimi == 5){
				level[2] = 5;
				life[2] = 900;
				damage[2] = 56;

				if(canPayCoinPlantMimi){
					coins -= 350;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 3200;
				nextLevelCoinPlantMimi = 400;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 3200 && coins >= 400){
			if(canLevelUpPlantMimi == 6){
				level[2] = 6;
				life[2] = 950;
				damage[2] = 60;

				if(canPayCoinPlantMimi){
					coins -= 400;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 4600;
				nextLevelCoinPlantMimi = 450;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 4600 && coins >= 450){
			if(canLevelUpPlantMimi == 7){
				level[2] = 7;
				life[2] = 1000;
				damage[2] = 64;

				if(canPayCoinPlantMimi){
					coins -= 450;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 7000;
				nextLevelCoinPlantMimi = 500;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 7000 && coins >= 500){
			if(canLevelUpPlantMimi == 8){
				level[2] = 8;
				life[2] = 1050;
				damage[2] = 68;

				if(canPayCoinPlantMimi){
					coins -= 500;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 9800;
				nextLevelCoinPlantMimi = 550;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 9800 && coins >= 550){
			if(canLevelUpPlantMimi == 9){
				level[2] = 9;
				life[2] = 1100;
				damage[2] = 72;

				if(canPayCoinPlantMimi){
					coins -= 550;
					canPayCoinPlantMimi = false;
				}

				nextLevelExpPlantMimi = 13200;
				nextLevelCoinPlantMimi = 600;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expPlantMimi >= 13200 && coins >= 600){
			if(canLevelUpPlantMimi == 10){
				level[2] = 10;
				life[2] = 1150;
				damage[2] = 76;

				if(canPayCoinPlantMimi){
					coins -= 600;
					canPayCoinPlantMimi = false;
				}
			}
		}
	}

	void waterMimiStats(){
		if(expWaterMimi < 400){
			level[3] = 1;
			life[3] = 800;
			damage[3] = 32;
		}
		if(expWaterMimi >= 400 && coins >= 100){
			if(canLevelUpWaterMimi == 2){
				level[3] = 2;
				life[3] = 850;
				damage[3] = 36;

				if(canPayCoinWaterMimi){
					coins -= 100;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 800;
				nextLevelCoinWaterMimi = 200;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 800 && coins >= 200){
			if(canLevelUpWaterMimi == 3){
				level[3] = 3;
				life[3] = 900;
				damage[3] = 40;

				if(canPayCoinWaterMimi){
					coins -= 200;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 1400;
				nextLevelCoinWaterMimi = 300;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 1400 && coins >= 300){
			if(canLevelUpWaterMimi == 4){
				level[3] = 4;
				life[3] = 950;
				damage[3] = 44;

				if(canPayCoinWaterMimi){
					coins -= 300;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 2200;
				nextLevelCoinWaterMimi = 350;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 2200 && coins >= 350){
			if(canLevelUpWaterMimi == 5){
				level[3] = 5;
				life[3] = 1000;
				damage[3] = 48;

				if(canPayCoinWaterMimi){
					coins -= 350;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 3200;
				nextLevelCoinWaterMimi = 400;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 3200 && coins >= 400){
			if(canLevelUpWaterMimi == 6){
				level[3] = 6;
				life[3] = 1050;
				damage[3] = 52;

				if(canPayCoinWaterMimi){
					coins -= 400;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 4600;
				nextLevelCoinWaterMimi = 450;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 4600 && coins >= 450){
			if(canLevelUpWaterMimi == 7){
				level[3] = 7;
				life[3] = 1100;
				damage[3] = 56;

				if(canPayCoinWaterMimi){
					coins -= 450;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 7000;
				nextLevelCoinWaterMimi = 500;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 7000 && coins >= 500){
			if(canLevelUpWaterMimi == 8){
				level[3] = 8;
				life[3] = 1150;
				damage[3] = 60;

				if(canPayCoinWaterMimi){
					coins -= 500;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 9800;
				nextLevelCoinWaterMimi = 550;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 9800 && coins >= 550){
			if(canLevelUpWaterMimi == 9){
				level[3] = 9;
				life[3] = 1200;
				damage[3] = 64;

				if(canPayCoinWaterMimi){
					coins -= 550;
					canPayCoinWaterMimi = false;
				}

				nextLevelExpWaterMimi = 13200;
				nextLevelCoinWaterMimi = 600;

				gameObject.GetComponent<SaveManager>().Save();
			}
		}
		if(expWaterMimi >= 13200 && coins >= 600){
			if(canLevelUpWaterMimi == 10){
				level[3] = 10;
				life[3] = 1250;
				damage[3] = 68;

				if(canPayCoinWaterMimi){
					coins -= 600;
					canPayCoinWaterMimi = false;
				}
			}
		}
	}

	public void canUpRayMimi(){
		if(level[0] == 1){
			canLevelUpRayMimi = 2;
		}
		if(level[0] == 2){
			canLevelUpRayMimi = 3;
		}
		if(level[0] == 3){
			canLevelUpRayMimi = 4;
		}
		if(level[0] == 4){
			canLevelUpRayMimi = 5;
		}
		if(level[0] == 5){
			canLevelUpRayMimi = 6;
		}
		if(level[0] == 6){
			canLevelUpRayMimi = 7;
		}
		if(level[0] == 7){
			canLevelUpRayMimi = 8;
		}
		if(level[0] == 8){
			canLevelUpRayMimi = 9;
		}
		if(level[0] == 9){
			canLevelUpRayMimi = 10;
		}
			
		canPayCoinRayMimi = true;
	}

	public void canUpFireMimi(){
		if(level[1] == 1){
			canLevelUpFireMimi = 2;
		}
		if(level[1] == 2){
			canLevelUpFireMimi = 3;
		}
		if(level[1] == 3){
			canLevelUpFireMimi = 4;
		}
		if(level[1] == 4){
			canLevelUpFireMimi = 5;
		}
		if(level[1] == 5){
			canLevelUpFireMimi = 6;
		}
		if(level[1] == 6){
			canLevelUpFireMimi = 7;
		}
		if(level[1] == 7){
			canLevelUpFireMimi = 8;
		}
		if(level[1] == 8){
			canLevelUpFireMimi = 9;
		}
		if(level[1] == 9){
			canLevelUpFireMimi = 10;
		}

		canPayCoinFireMimi = true;
	}

	public void canUpPlantMimi(){
		if(level[2] == 1){
			canLevelUpPlantMimi = 2;
		}
		if(level[2] == 2){
			canLevelUpPlantMimi = 3;
		}
		if(level[2] == 3){
			canLevelUpPlantMimi = 4;
		}
		if(level[2] == 4){
			canLevelUpPlantMimi = 5;
		}
		if(level[2] == 5){
			canLevelUpPlantMimi = 6;
		}
		if(level[2] == 6){
			canLevelUpPlantMimi = 7;
		}
		if(level[2] == 7){
			canLevelUpPlantMimi = 8;
		}
		if(level[2] == 8){
			canLevelUpPlantMimi = 9;
		}
		if(level[2] == 9){
			canLevelUpPlantMimi = 10;
		}

		canPayCoinPlantMimi = true;
	}

	public void canUpWaterMimi(){
		if(level[3] == 1){
			canLevelUpWaterMimi = 2;
		}
		if(level[3] == 2){
			canLevelUpWaterMimi = 3;
		}
		if(level[3] == 3){
			canLevelUpWaterMimi = 4;
		}
		if(level[3] == 4){
			canLevelUpWaterMimi = 5;
		}
		if(level[3] == 5){
			canLevelUpWaterMimi = 6;
		}
		if(level[3] == 6){
			canLevelUpWaterMimi = 7;
		}
		if(level[3] == 7){
			canLevelUpWaterMimi = 8;
		}
		if(level[3] == 8){
			canLevelUpWaterMimi = 9;
		}
		if(level[3] == 9){
			canLevelUpWaterMimi = 10;
		}

		canPayCoinWaterMimi = true;
	}
}
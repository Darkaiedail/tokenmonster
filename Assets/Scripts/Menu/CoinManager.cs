﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour {

	[HideInInspector]
	public int coins;
	public Text coinText;

	void Start(){
		earnCoins();
	}

	public void earnCoins(){
		coins = GameObject.Find("Manager").GetComponent<EarnManager>().coins;
		earnTextCoins();
	}

	void earnTextCoins(){
		string coinsStr = coins.ToString("00000");

		coinText.text = coinsStr;
	}
}

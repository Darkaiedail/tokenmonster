﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveManager : MonoBehaviour {

	public void Save(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		PlayerData data = new PlayerData();
		//data.nextLevel = gameObject.GetComponent<EarnManager>().nextLevel;
		data.coins = gameObject.GetComponent<EarnManager>().coins;
		data.screws = gameObject.GetComponent<EarnManager>().screws;
		data.nuts = gameObject.GetComponent<EarnManager>().nuts;

		data.expRayMimi = gameObject.GetComponent<EarnManager>().expRayMimi;
		data.lifeRayMimi = gameObject.GetComponent<EarnManager>().life[0];
		data.damageRayMimi = gameObject.GetComponent<EarnManager>().damage[0];
		data.levelRayMimi = gameObject.GetComponent<EarnManager>().level[0];
		data.nextLevelExpRayMimi = gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi;
		data.nextLevelCoinRayMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi;

		data.expFireMimi = gameObject.GetComponent<EarnManager>().expFireMimi;
		data.lifeFireMimi = gameObject.GetComponent<EarnManager>().life[1];
		data.damageFireMimi = gameObject.GetComponent<EarnManager>().damage[1];
		data.levelFireMimi = gameObject.GetComponent<EarnManager>().level[1];
		data.nextLevelExpFireMimi = gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi;
		data.nextLevelCoinFireMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi;

		data.expPlantMimi = gameObject.GetComponent<EarnManager>().expPlantMimi;
		data.lifePlantMimi = gameObject.GetComponent<EarnManager>().life[2];
		data.damagePlantMimi = gameObject.GetComponent<EarnManager>().damage[2];
		data.levelPlantMimi = gameObject.GetComponent<EarnManager>().level[2];
		data.nextLevelExpPlantMimi = gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi;
		data.nextLevelCoinPlantMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi;

		data.expWaterMimi = gameObject.GetComponent<EarnManager>().expWaterMimi;
		data.lifeWaterMimi = gameObject.GetComponent<EarnManager>().life[3];
		data.damageWaterMimi = gameObject.GetComponent<EarnManager>().damage[3];
		data.levelWaterMimi = gameObject.GetComponent<EarnManager>().level[3];
		data.nextLevelExpWaterMimi = gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi;
		data.nextLevelCoinWaterMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi;

		bf.Serialize(file, data);
		file.Close();
	}

	public void Load(){
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();

			//gameObject.GetComponent<EarnManager>().nextLevel = data.nextLevel;
			gameObject.GetComponent<EarnManager>().coins = data.coins;
			gameObject.GetComponent<EarnManager>().screws = data.screws;
			gameObject.GetComponent<EarnManager>().nuts = data.nuts;

			gameObject.GetComponent<EarnManager>().expRayMimi = data.expRayMimi;
			gameObject.GetComponent<EarnManager>().life[0] = data.lifeRayMimi;
			gameObject.GetComponent<EarnManager>().damage[0] = data.damageRayMimi;
			gameObject.GetComponent<EarnManager>().level[0] = data.levelRayMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi = data.nextLevelExpRayMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi = data.nextLevelCoinRayMimi;

			gameObject.GetComponent<EarnManager>().expFireMimi = data.expFireMimi;
			gameObject.GetComponent<EarnManager>().life[1] = data.lifeFireMimi;
			gameObject.GetComponent<EarnManager>().damage[1] = data.damageFireMimi;
			gameObject.GetComponent<EarnManager>().level[1] = data.levelFireMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi = data.nextLevelExpFireMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi = data.nextLevelCoinFireMimi;

			gameObject.GetComponent<EarnManager>().expPlantMimi = data.expPlantMimi;
			gameObject.GetComponent<EarnManager>().life[2] = data.lifePlantMimi;
			gameObject.GetComponent<EarnManager>().damage[2] = data.damagePlantMimi;
			gameObject.GetComponent<EarnManager>().level[2] = data.levelPlantMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi = data.nextLevelExpPlantMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi = data.nextLevelCoinPlantMimi;

			gameObject.GetComponent<EarnManager>().expWaterMimi = data.expWaterMimi;
			gameObject.GetComponent<EarnManager>().life[3] = data.lifeWaterMimi;
			gameObject.GetComponent<EarnManager>().damage[3] = data.damageWaterMimi;
			gameObject.GetComponent<EarnManager>().level[3] = data.levelWaterMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi = data.nextLevelExpWaterMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi = data.nextLevelCoinWaterMimi;

			GameObject.Find("ManagerMenu").GetComponent<CoinManager>().coins = data.coins;

			GameObject.Find("ManagerMenu").GetComponent<CoinManager>().earnCoins();
		}
	}

	public void ResetGame(){
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			//BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			//PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();

			//gameObject.GetComponent<EarnManager>().nextLevel = 0;
			gameObject.GetComponent<EarnManager>().coins = 0;
			gameObject.GetComponent<EarnManager>().screws = 0;
			gameObject.GetComponent<EarnManager>().nuts = 0;

			gameObject.GetComponent<EarnManager>().expRayMimi = 0;
			gameObject.GetComponent<EarnManager>().life[0] = 900;
			gameObject.GetComponent<EarnManager>().damage[0] = 36;
			gameObject.GetComponent<EarnManager>().level[0] = 1;
			gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi = 400;
			gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi = 100;

			gameObject.GetComponent<EarnManager>().expFireMimi = 0;
			gameObject.GetComponent<EarnManager>().life[1] = 1000;
			gameObject.GetComponent<EarnManager>().damage[1] = 28;
			gameObject.GetComponent<EarnManager>().level[1] = 1;
			gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi = 400;
			gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi = 100;

			gameObject.GetComponent<EarnManager>().expPlantMimi = 0;
			gameObject.GetComponent<EarnManager>().life[2] = 700;
			gameObject.GetComponent<EarnManager>().damage[2] = 40;
			gameObject.GetComponent<EarnManager>().level[2] = 1;
			gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi = 400;
			gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi = 100;

			gameObject.GetComponent<EarnManager>().expWaterMimi = 0;
			gameObject.GetComponent<EarnManager>().life[3] = 800;
			gameObject.GetComponent<EarnManager>().damage[3] = 32;
			gameObject.GetComponent<EarnManager>().level[3] = 1;
			gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi = 400;
			gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi = 100;

			GameObject.Find("ManagerMenu").GetComponent<CoinManager>().coins = 0;

			GameObject.Find("ManagerMenu").GetComponent<CoinManager>().earnCoins();

			Save();
		}

		PlayerPrefs.DeleteAll();
	}
}

[Serializable]
class PlayerData{

	//public int nextLevel;
	public int coins;
	public int screws;
	public int nuts;

	public int expRayMimi;
	public int lifeRayMimi;
	public int damageRayMimi;
	public int levelRayMimi;

	public int expFireMimi;
	public int lifeFireMimi;
	public int damageFireMimi;
	public int levelFireMimi;

	public int expPlantMimi;
	public int lifePlantMimi;
	public int damagePlantMimi;
	public int levelPlantMimi;

	public int expWaterMimi;
	public int lifeWaterMimi;
	public int damageWaterMimi;
	public int levelWaterMimi;

	public int nextLevelExpRayMimi = 400;
	public int nextLevelCoinRayMimi = 100;

	public int nextLevelExpFireMimi = 400;
	public int nextLevelCoinFireMimi = 100;

	public int nextLevelExpPlantMimi = 400;
	public int nextLevelCoinPlantMimi = 100;

	public int nextLevelExpWaterMimi = 400;
	public int nextLevelCoinWaterMimi = 100;
}

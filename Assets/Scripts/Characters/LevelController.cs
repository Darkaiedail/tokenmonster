﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

	public enum TypeOfChar {RayMimi, FireMimi, PlantMimi, WaterMimi};
	public TypeOfChar charName;

	public int exp;
	public int life;
	public int damage;

	void Start(){
		switch(charName){
		case TypeOfChar.RayMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expRayMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[0];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[0];
			break;
		case TypeOfChar.FireMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expFireMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[1];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[1];
			break;
		case TypeOfChar.PlantMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expPlantMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[2];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[2];
			break;
		case TypeOfChar.WaterMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expWaterMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[3];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[3];
			break;
		}
	}
}

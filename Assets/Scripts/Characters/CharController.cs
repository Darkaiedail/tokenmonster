﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharController : MonoBehaviour {

	public enum TypeOfChar {Ray, Fire, Plant, Water};
	public TypeOfChar charType;

	public enum ClassOfChar{Tank, Special, Damage, Balance};
	public ClassOfChar charClass;

	public float charDamage;
	public float life;
	public float health;
	public float heartsValue;

	public GameObject enemy;
	public GameObject[] enemyArray;

	private int numYellowTokens;
	private int numRedTokens;
	private int numGreenTokens;
	private int numBlueTokens; 

	public GameObject[] healthBar;
	public Text damagePrefab;
	private Text damageTextPrefab;
	public GameObject canvas;

	public GameObject[] damageText;
	public int h;

	[HideInInspector]
	public bool firstRound;

	[HideInInspector]
	public bool mazEnd;

	public bool changeStats;

	public void numberYellowTokensDestroy(int numTokens){
		numYellowTokens += numTokens;
		print("numYellowTokens " + numYellowTokens);
	}

	public void numberRedTokensDestroy(int numTokens){
		numRedTokens += numTokens;
		print("numRedTokens " + numRedTokens);
	}

	public void numberGreenTokensDestroy(int numTokens){
		numGreenTokens += numTokens;
		print("numGreenTokens " + numGreenTokens);
	}

	public void numberBlueTokensDestroy(int numTokens){
		numBlueTokens += numTokens;
		print("numBlueTokens " + numBlueTokens);
	}

	public void restartNumTokens(){
		numYellowTokens = 0;
		numRedTokens = 0;
		numGreenTokens = 0;
		numBlueTokens = 0;
	}

	public void enemySelected(GameObject enemySelec){
		enemy = enemySelec;
	}

	void Awake(){
		canvas = GameObject.Find("Canvas");
		healthBar = GameObject.FindGameObjectsWithTag("HealthBar");
		damageText = GameObject.FindGameObjectsWithTag("DamageText");
	}

	void selectFirstRandomEnemy(){
		enemyArray = GameObject.FindGameObjectsWithTag("Enemy");

		int i = GameObject.Find("Board").GetComponent<BoardManager>().randomEnemy;
		enemySelected(enemyArray[i]);
		gameObject.GetComponent<HabilityController>().enemySelected(enemyArray[i]);
		enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(false);
		enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(true);
	}

	public void selectRandomEnemy(){
		enemyArray = null;

		StartCoroutine("findEnemies");
	}

	IEnumerator findEnemies(){
		yield return new WaitForSeconds(0.5f);

		if(!mazEnd){
			enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
			int i = 0;
			enemySelected(enemyArray[i]);
			gameObject.GetComponent<HabilityController>().enemySelected(enemyArray[i]);
			enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(false);
			enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(true);
		}
	}

	void Update(){
		life = gameObject.GetComponent<LevelController>().life;
		health = life;
		charDamage = gameObject.GetComponent<LevelController>().damage;

		if(!mazEnd){
			if(firstRound){
				selectFirstRandomEnemy();
				firstRound = false;
			}

			if(charType == CharController.TypeOfChar.Ray){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Ray && numYellowTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Fire && numYellowTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Plant && numYellowTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Water && numYellowTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
				}
			}
			else if(charType == CharController.TypeOfChar.Fire){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Ray && numRedTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Fire && numRedTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Plant && numRedTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Water && numRedTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
				}
			}
			else if(charType == CharController.TypeOfChar.Plant){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Ray && numGreenTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Fire && numGreenTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Plant && numGreenTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Water && numGreenTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
				}
			}
			else if(charType == CharController.TypeOfChar.Water){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Ray && numBlueTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Fire && numBlueTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Plant && numBlueTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == EnemyController.typeOfEnemy.Water && numBlueTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);
						printDamage(totalDamage);

						restartNumTokens();
					}
				}
			}

			if(health > life){
				health = life;
			}

			if(health <= 0){
				die();
			}
		}
	}

	public void defeat(float damage){
		health -= damage;

		float healthSize = health/life;
		healthBar[h].GetComponent<Scrollbar>().size = healthSize;
	}

	public void cure(int hearts){
		float totalCure = hearts * heartsValue;
		health += totalCure;

		float healthSize = health/life;
		healthBar[h].GetComponent<Scrollbar>().size = healthSize;

		printCharCure(totalCure);
	}

	void die(){
		Destroy(this.gameObject);
		healthBar[h].GetComponent<Scrollbar>().gameObject.SetActive(false);

		for(int i = 0; i < enemyArray.Length; i++){
			enemyArray[i].GetComponent<EnemyController>().findCharacters();
		}
	}

	void printDamage(float damage){

		damageTextPrefab = Instantiate(damagePrefab,enemy.transform.position, Quaternion.identity) as Text;
		damageTextPrefab.transform.SetParent(canvas.transform, false);

		damageTextPrefab.transform.position = enemy.transform.position;

		damageTextPrefab.gameObject.SetActive(true);
		iTween.MoveAdd(damageTextPrefab.gameObject, iTween.Hash("x", .5f,
			"time", .5f, "easetype","easeOutBounce"));
		iTween.ScaleAdd(damageTextPrefab.gameObject, iTween.Hash("y", .5f, "x", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = damage.ToString("00");
		damageTextPrefab.text = turnsStr;

		StartCoroutine("desactiveDamage");
	}

	IEnumerator desactiveDamage(){
		yield return new WaitForSeconds(1.0f);

		damageTextPrefab.gameObject.SetActive(false);
		Destroy(damageTextPrefab);
		Destroy(GameObject.Find("EnemyDamage(Clone)"));
	}

	public void printCharDamage(float damage){
		damageText[h].GetComponent<Text>().gameObject.SetActive(true);
		iTween.MoveAdd(damageText[h].gameObject, iTween.Hash("y", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));
		iTween.ScaleAdd(damageText[h].gameObject, iTween.Hash("y", .005f, "x", .005f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = damage.ToString("00");
		damageText[h].GetComponent<Text>().text = "-" + turnsStr;

		StartCoroutine("desactiveCharDamage");
	}

	void printCharCure(float heart){
		damageText[h].GetComponent<Text>().gameObject.SetActive(true);
		iTween.MoveAdd(damageText[h].gameObject, iTween.Hash("y", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));
		iTween.ScaleAdd(damageText[h].gameObject, iTween.Hash("y", .005f, "x", .005f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = heart.ToString("00");
		damageText[h].GetComponent<Text>().text = "+" + turnsStr;

		StartCoroutine("desactiveCharDamage");
	}

	IEnumerator desactiveCharDamage(){
		yield return new WaitForSeconds(1.0f);

		damageText[h].GetComponent<Text>().gameObject.SetActive(false);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HabilityController : MonoBehaviour {

	public enum TypeOfHability {Defeat, Shake, Bomb, BrokeTokens};
	public TypeOfHability charHability;

	private int numYellowTokens;
	private int numRedTokens;
	private int numGreenTokens;
	private int numBlueTokens; 

	public int tokensToHability;

	public GameObject enemy;
	public float timeToRestartEnemyStats;
	private float startEnemyDamage;
	private bool firstRound = true;

	private GameObject[] gems;

	//public GameObject[] habilityBar;
	public int h;

	public GameObject selector;
	private GameObject selectorInstance;
	private bool isInstantiateYellow = false;
	private bool isInstantiateRed = false;
	private bool isInstantiateGreen = false;
	private bool isInstantiateBlue = false;

//	void Awake(){
//		//habilityBar = GameObject.FindGameObjectsWithTag("HabilityBar");
//	}

	void Start(){
		gems = GameObject.FindGameObjectsWithTag("Gem");
		startEnemyDamage = enemy.GetComponent<EnemyController>().enemyDamage;
	}

	public void enemySelected(GameObject enemySelec){
		enemy = enemySelec;
	}

	public void numberYellowTokensToHability(int numTokens){
		numYellowTokens += numTokens;
		//incrementHabilityBar();
		isInstantiateYellow = false;
	}

	public void numberRedTokensToHability(int numTokens){
		numRedTokens += numTokens;
		//incrementHabilityBar();
		isInstantiateRed = false;
	}

	public void numberGreenTokensToHability(int numTokens){
		numGreenTokens += numTokens;
		//incrementHabilityBar();
		isInstantiateGreen = false;
	}

	public void numberBlueTokensToHability(int numTokens){
		numBlueTokens += numTokens;
		//incrementHabilityBar();
		isInstantiateBlue = false;
	}

	void restartTokens(){
		numYellowTokens = 0;
		numRedTokens = 0;
		numGreenTokens = 0;
		numBlueTokens = 0; 
		//incrementHabilityBar();
	}

	void Update () {
		if(firstRound && enemy != null){
			startEnemyDamage = enemy.GetComponent<EnemyController>().enemyDamage;
			firstRound = false;
		}

		if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Ray && numYellowTokens >= tokensToHability){
			beguinHability();
			print("gooooo");
			if(!isInstantiateYellow){
				selectorInstance = Instantiate(selector, this.transform.position, Quaternion.identity) as GameObject;
				isInstantiateYellow = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Fire && numRedTokens >= tokensToHability){
			beguinHability();
			if(!isInstantiateRed){
				selectorInstance = Instantiate(selector, this.transform.position, Quaternion.identity) as GameObject;
				isInstantiateRed = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Plant && numGreenTokens >= tokensToHability){
			beguinHability();
			if(!isInstantiateGreen){
				selectorInstance = Instantiate(selector, this.transform.position, Quaternion.identity) as GameObject;
				isInstantiateGreen = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Water && numBlueTokens >= tokensToHability){
			beguinHability();
			if(!isInstantiateBlue){
				selectorInstance = Instantiate(selector, this.transform.position, Quaternion.identity) as GameObject;
				isInstantiateBlue = true;
			}
		}
	}

	void doHability(){
		if(enemy != null){
			switch(charHability){
			case TypeOfHability.Defeat:
				print("defeat");
				restartTokens();

				enemy.GetComponent<EnemyController>().enemyDamage -= enemy.GetComponent<EnemyController>().enemyDamage * 0.2f;
				StartCoroutine("restartEnemyDamage");

				Destroy(GameObject.Find("SelectorYellow(Clone)"));
				Destroy(GameObject.Find("SelectorRed(Clone)"));
				Destroy(GameObject.Find("SelectorGreen(Clone)"));
				Destroy(GameObject.Find("SelectorBlue(Clone)"));

				break;

			case TypeOfHability.Shake:
				print("shake");
				restartTokens();

				if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Ray){
					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().destroyYellow();

						Destroy(GameObject.Find("SelectorYellow(Clone)"));
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Fire){
					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().destroyRed();

						Destroy(GameObject.Find("SelectorRed(Clone)"));
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Plant){
					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().destroyGreen();

						Destroy(GameObject.Find("SelectorGreen(Clone)"));
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharController.TypeOfChar.Water){
					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().destroyBlue();

						Destroy(GameObject.Find("SelectorBlue(Clone)"));
					}
				}

				break;

			case TypeOfHability.Bomb:
				print("bomb");
				restartTokens();

				break;

			case TypeOfHability.BrokeTokens:
				print("broke");
				restartTokens();

				break;
			}
		}
	}

	void beguinHability(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit = new RaycastHit ();

		if (Physics.Raycast (ray, out hit, 1000)) {
			if(Input.GetMouseButtonDown(0) && hit.collider.gameObject.CompareTag ("Box")){
				doHability();
			}
		}
	}

	IEnumerator restartEnemyDamage(){
		yield return new WaitForSeconds(timeToRestartEnemyStats);
		enemy.GetComponent<EnemyController>().enemyDamage = startEnemyDamage;
	}

//	void incrementHabilityBar(){
//		switch(this.gameObject.GetComponent<CharController>().charType){
//		case CharController.TypeOfChar.Ray:
//			float habilityRaySize = numYellowTokens/tokensToHability;
//			//habilityBar[h].GetComponent<Scrollbar>().size = habilityRaySize;
//
//			break;
//
//		case CharController.TypeOfChar.Fire:
//			float habilityFireSize = numRedTokens/tokensToHability;
//			//habilityBar[h].GetComponent<Scrollbar>().size = habilityFireSize;
//
//			break;
//
//		case CharController.TypeOfChar.Plant:
//			float habilityPlantSize = numGreenTokens/tokensToHability;
//			//habilityBar[h].GetComponent<Scrollbar>().size = habilityPlantSize;
//
//			break;
//
//		case CharController.TypeOfChar.Water:
//			float habilityWaterSize = numBlueTokens/tokensToHability;
//			//habilityBar[h].GetComponent<Scrollbar>().size = habilityWaterSize;
//
//			break;
//		}
//	}
}

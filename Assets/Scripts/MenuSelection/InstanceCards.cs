﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InstanceCards : MonoBehaviour {

	public GameObject[] cards;
	public RectTransform panel;

//	private Vector3 mousePos;
//	private Vector3 objectPos;

	private bool createdMimi = false;

	public GameObject[] selectionBoxes;
	private GameObject card;

	[HideInInspector]
	public bool[] instantiatedCard = new bool[100];

	void Update(){
		if(Input.GetButtonDown ("Fire1") && !createdMimi){
//			mousePos = Input.mousePosition;
//			objectPos = Camera.main.ScreenToWorldPoint(mousePos);
//			objectPos.z = 0.0f;

			createdMimi = true;	
		}
	}

	public void returnScene(){
		SceneManager.LoadScene(2);
	}

	public void InstanceMimiCard(){
		if(!instantiatedCard[0]){
			if(!selectionBoxes[0].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[0], selectionBoxes[0].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[0].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[0]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().i = 0;
			}
			else if(!selectionBoxes[1].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[0], selectionBoxes[1].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[1].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[1]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().j = 0;
			}
			else if(!selectionBoxes[2].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[0], selectionBoxes[2].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[2].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[2]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().k = 0;
			}
			else if(!selectionBoxes[3].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[0], selectionBoxes[3].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[3].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[3]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().h = 0;
			}

			instantiatedCard[0] = true;
		}
	}

	public void InstanceFireMimiCard(){
		if(!instantiatedCard[1]){
			if(!selectionBoxes[0].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[1], selectionBoxes[0].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[0].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[0]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().i = 1;
			}
			else if(!selectionBoxes[1].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[1], selectionBoxes[1].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[1].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[1]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().j = 1;
			}
			else if(!selectionBoxes[2].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[1], selectionBoxes[2].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[2].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[2]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().k = 1;
			}
			else if(!selectionBoxes[3].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[1], selectionBoxes[3].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[3].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[3]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().h = 1;
			}

			instantiatedCard[1] = true;
		}
	}

	public void InstancePlantMimiCard(){
		if(!instantiatedCard[2]){
			if(!selectionBoxes[0].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[2], selectionBoxes[0].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[0].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[0]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().i = 2;
			}
			else if(!selectionBoxes[1].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[2], selectionBoxes[1].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[1].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[1]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().j = 2;
			}
			else if(!selectionBoxes[2].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[2], selectionBoxes[2].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[2].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[2]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().k = 2;
			}
			else if(!selectionBoxes[3].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[2], selectionBoxes[3].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[3].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[3]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().h = 2;
			}

			instantiatedCard[2] = true;
		}
	}

	public void InstanceWaterMimiCard(){
		if(!instantiatedCard[3]){
			if(!selectionBoxes[0].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[3], selectionBoxes[0].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[0].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[0]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().i = 3;
			}
			else if(!selectionBoxes[1].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[3], selectionBoxes[1].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[1].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[1]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().j = 3;
			}
			else if(!selectionBoxes[2].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[3], selectionBoxes[2].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[2].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[2]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().k = 3;
			}
			else if(!selectionBoxes[3].GetComponent<SelectionBoxController>().isFill){
				card = Instantiate(cards[3], selectionBoxes[3].transform.position, Quaternion.identity) as GameObject;

				iTween.MoveTo(card.gameObject, iTween.Hash("position", selectionBoxes[3].transform.position,
					"easetype","easeOutQuint"));

				card.GetComponent<CardController>().boxSelection(selectionBoxes[3]);

				GameObject.Find("ManagerSelection").GetComponent<ManagerSelection>().h = 3;
			}

			instantiatedCard[3] = true;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CardController : MonoBehaviour {

	public GameObject panel;
	public GameObject box;

	void Start(){
		panel = GameObject.Find("Panel");
	}

//	void OnTriggerEnter(Collider obj){
//		if(obj.gameObject.CompareTag("Box") && !obj.gameObject.GetComponent<SelectionBoxController>().isFill){
//			iTween.MoveTo(gameObject, iTween.Hash("position", obj.gameObject.transform.position,
//				"easetype","easeOutQuint"));
//
//			panel.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
//		}
//
//		if(obj.gameObject.CompareTag("Box") && obj.gameObject.GetComponent<SelectionBoxController>().isFill){
//			
//		}
//	}

	public void boxSelection(GameObject obj){
		box = obj;
	}

	void Update(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit = new RaycastHit ();

		if (Physics.Raycast (ray, out hit, 1000)) {
			if(Input.GetMouseButtonDown(0) && hit.collider.gameObject.CompareTag ("Card")){
				Destroy(hit.collider.gameObject);
				box.GetComponent<SelectionBoxController>().isEmpty();

				for(int i = 0; i < GameObject.Find("ManagerMenu").GetComponent<InstanceCards>().instantiatedCard.Length; i++){
					GameObject.Find("ManagerMenu").GetComponent<InstanceCards>().instantiatedCard[i] = false;
				}
			}
		}
	}
}

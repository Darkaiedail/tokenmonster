﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollPanelController : MonoBehaviour {

	//hold the scroll panel
	public RectTransform panel; 
	public Button[] bttn;
	//center to compare the distance for each button
	public RectTransform center;

	//all buttons distance to the center
	private float[] distance;
	private float[] distReposition;
	private bool dragging = false;
	//distance between the buttons
	private int bttnDistance;
	//hold the number of the button, with smallest distance to center
	private int minButtonNum;
	private int bttnLenght;

	void Start(){
		bttnLenght = bttn.Length;
		distance = new float[bttnLenght];
		distReposition = new float[bttnLenght];

		//get distance between buttons
		bttnDistance = (int)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x - bttn[0].GetComponent<RectTransform>().anchoredPosition.x);
	}

	void Update(){
		for(int i = 0; i < bttn.Length; i++){
			distReposition[i] = center.GetComponent<RectTransform>().position.x - bttn[i].GetComponent<RectTransform>().position.x;
			distance[i] = Mathf.Abs(center.transform.position.x - bttn[i].transform.position.x);

			if(distReposition[i] > 1000){
				float currX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
				float currY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

				Vector2 newAnchoredPosition = new Vector2 (currX + (bttnLenght + bttnDistance), currY);
				bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
			}
		}

		float minDistace = Mathf.Min(distance);

		for(int j = 0; j < bttn.Length; j++){
			if(minDistace == distance[j]){
				minButtonNum = j;
			}
		}

		if(!dragging){
			LerpToBttn(minButtonNum * - bttnDistance);
			//LerpToBttn(Mathf.RoundToInt(- bttn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.x));
		}
	}

	void LerpToBttn(int position){
		float newX = Mathf.Lerp(panel.anchoredPosition.x,position,Time.deltaTime * 2.5f);
		Vector2 newPosition = new Vector2 (newX, panel.anchoredPosition.y);

		panel.anchoredPosition = newPosition;
	}

	public void StartDrag(){
		dragging = true;
	}

	public void EndDragg(){
		dragging = false;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

	public GameObject loadImage;

	public int nextLevel;
	public GameObject[] buttons;

	private bool firstTimeLevelOne;
	private bool firstTimeLevelTwo;

	public void loadLevel (int level) {
		loadImage.SetActive(true);
		SceneManager.LoadScene(3);

		//GameObject.Find("Manager").GetComponent<EarnManager>().sceneLevel = level;
	}

	public void backMenu(){
		loadImage.SetActive(true);
		SceneManager.LoadScene(0);
	}

	void Start(){
		nextLevel = GameObject.Find("Manager").GetComponent<EarnManager>().nextLevel;

		if(nextLevel == 2){
			if(!firstTimeLevelOne){
				for(int i = 0; i < nextLevel - 1; i++){
					buttons[i].SetActive(true);
				}

				firstTimeLevelOne = true;
			}
		}
		if(nextLevel == 3){
			if(!firstTimeLevelTwo){
				for(int i = 0; i < nextLevel - 1; i++){
					buttons[i].SetActive(true);
				}

				firstTimeLevelTwo = true;
			}
		}
	}
}

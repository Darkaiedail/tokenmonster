﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChestController : MonoBehaviour {

	public int coins;
	public Image coinsImg;
	public Text coinsText;

	public int screws;
	public Image screwsImg;
	public Text screwsText;

	public int nuts;
	public Image nutsImg;
	public Text nutsText;

//	float Choose (float[] probs) {
//
//		float total = 0;
//
//		foreach (float elem in probs) {
//			total += elem;
//		}
//
//		float randomPoint = Random.value * total;
//
//		for (int i= 0; i < probs.Length; i++) {
//			if (randomPoint < probs[i]) {
//				return i;
//			}
//			else {
//				randomPoint -= probs[i];
//			}
//		}
//		return probs.Length - 1;
//	}

	public void recieveReward (){
		coinsText.gameObject.SetActive(true);
		printEarnCoins();
		iTween.MoveAdd(coinsText.gameObject, iTween.Hash("y", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		coinsImg.gameObject.SetActive(true);
		iTween.MoveAdd(coinsImg.gameObject, iTween.Hash("y", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		GameObject.Find("Manager").GetComponent<EarnManager>().coins += coins;

		StartCoroutine("deactiveEarnCoins");

		int number = Random.Range(0,2);
		if(number == 0){
			screwsText.gameObject.SetActive(true);
			printEarnScrews();
			iTween.MoveAdd(screwsText.gameObject, iTween.Hash("y", .5f,
				"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

			screwsImg.gameObject.SetActive(true);
			iTween.MoveAdd(screwsImg.gameObject, iTween.Hash("y", .5f,
				"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

			GameObject.Find("Manager").GetComponent<EarnManager>().screws += screws;
		}

		int numberTwo = Random.Range(0,4);
		if(numberTwo == 0){
			nutsText.gameObject.SetActive(true);
			printEarnNuts();
			iTween.MoveAdd(nutsText.gameObject, iTween.Hash("y", .5f,
				"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

			nutsImg.gameObject.SetActive(true);
			iTween.MoveAdd(nutsImg.gameObject, iTween.Hash("y", .5f,
				"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

			GameObject.Find("Manager").GetComponent<EarnManager>().nuts += nuts;
		}
	}

	void printEarnCoins(){
		string coinsStr = coins.ToString("00");

		coinsText.text = "+ " + coinsStr;
	}
		
	void printEarnScrews(){
		string screwStr = screws.ToString("00");

		screwsText.text = "+ " + screwStr;
	}

	void printEarnNuts(){
		string nutsStr = nuts.ToString("00");

		nutsText.text = "+ " + nutsStr;
	}

	IEnumerator deactiveEarnCoins(){
		yield return new WaitForSeconds(.6f);

		coinsText.gameObject.SetActive(false);
		coinsImg.gameObject.SetActive(false);
		screwsText.gameObject.SetActive(false);
		screwsImg.gameObject.SetActive(false);
		nutsText.gameObject.SetActive(false);
		nutsImg.gameObject.SetActive(false);
	}

}
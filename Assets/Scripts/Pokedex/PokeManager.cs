﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PokeManager : MonoBehaviour {

	private bool active;
	private bool haveCharacter;

	public Text[] nextLevelExpText;
	public Text[] nextLevelPayCoins;

	void Start(){
		printStatsRayMimi();
		printStatsFireMimi();
		printStatsPlantMimi();
		printStatsWaterMimi();
	}

	public void showPanel(GameObject panel){
		if(!active){
			panel.SetActive(true);
			StartCoroutine("activate");
		}
		if(active){
			panel.SetActive(false);
			StartCoroutine("desactivate");
		}
	}

	IEnumerator activate(){
		yield return new WaitForSeconds(.05f);

		active = true;
	}

	IEnumerator desactivate(){
		yield return new WaitForSeconds(.05f);

		active = false;
	}

	public void returnScene(){
		SceneManager.LoadScene(0);
	}

	void printStatsRayMimi(){
		string levelExpStr = "/" + GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelExpRayMimi.ToString("000");
		string levelCoinStr = GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelCoinRayMimi.ToString("000");

		nextLevelExpText[0].text = levelExpStr;
		nextLevelPayCoins[0].text = levelCoinStr;
	}

	void printStatsFireMimi(){
		string levelExpStr = "/" + GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelExpFireMimi.ToString("000");
		string levelCoinStr = GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelCoinFireMimi.ToString("000");

		nextLevelExpText[1].text = levelExpStr;
		nextLevelPayCoins[1].text = levelCoinStr;
	}

	void printStatsPlantMimi(){
		string levelExpStr = "/" + GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelExpPlantMimi.ToString("000");
		string levelCoinStr = GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelCoinPlantMimi.ToString("000");

		nextLevelExpText[2].text = levelExpStr;
		nextLevelPayCoins[2].text = levelCoinStr;
	}

	void printStatsWaterMimi(){
		string levelExpStr = "/" + GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelExpWaterMimi.ToString("000");
		string levelCoinStr = GameObject.Find("Manager").GetComponent<EarnManager>().nextLevelCoinWaterMimi.ToString("000");

		nextLevelExpText[3].text = levelExpStr;
		nextLevelPayCoins[3].text = levelCoinStr;
	}
}

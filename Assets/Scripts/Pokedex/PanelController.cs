﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PanelController : MonoBehaviour {

	public enum TypeOfChar {RayMimi, FireMimi, PlantMimi, WaterMimi};
	public TypeOfChar charName;

	public int level;
	public int life;
	public int damage;
	public int exp;
	public int coins;

	public Text levelText;
	public Text lifeText;
	public Text damageText;
	public Text expText;
	public Text coinsText;

	void Start(){
		switch(charName){
		case TypeOfChar.RayMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expRayMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[0];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[0];
			level = GameObject.Find("Manager").GetComponent<EarnManager>().level[0];
			coins =  GameObject.Find("Manager").GetComponent<EarnManager>().coins;
			printStats();
			break;

		case TypeOfChar.FireMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expFireMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[1];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[1];
			level = GameObject.Find("Manager").GetComponent<EarnManager>().level[1];
			coins =  GameObject.Find("Manager").GetComponent<EarnManager>().coins;
			printStats();
			break;

		case TypeOfChar.PlantMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expPlantMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[2];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[2];
			level = GameObject.Find("Manager").GetComponent<EarnManager>().level[2];
			coins =  GameObject.Find("Manager").GetComponent<EarnManager>().coins;
			printStats();
			break;

		case TypeOfChar.WaterMimi:
			exp = GameObject.Find("Manager").GetComponent<EarnManager>().expWaterMimi;
			life = GameObject.Find("Manager").GetComponent<EarnManager>().life[3];
			damage = GameObject.Find("Manager").GetComponent<EarnManager>().damage[3];
			level = GameObject.Find("Manager").GetComponent<EarnManager>().level[3];
			coins =  GameObject.Find("Manager").GetComponent<EarnManager>().coins;
			printStats();
			break;
		}
	}

	void printStats(){
		string levelStr = level.ToString("000");

		levelText.text = levelStr;

		string lifeStr = life.ToString("000");

		lifeText.text = lifeStr;

		string damageStr = damage.ToString("000");

		damageText.text = damageStr;

		string expStr = exp.ToString("000");

		expText.text = expStr;

		string coinsStr = coins.ToString("00000");

		coinsText.text = coinsStr;
	}

	public void levelUpRayMimi(){
		GameObject.Find("Manager").GetComponent<EarnManager>().canUpRayMimi();
		StartCoroutine("print");
	}

	public void levelUpFireMimi(){
		GameObject.Find("Manager").GetComponent<EarnManager>().canUpFireMimi();
		StartCoroutine("print");
	}

	public void levelUpPlantMimi(){
		GameObject.Find("Manager").GetComponent<EarnManager>().canUpPlantMimi();
		StartCoroutine("print");
	}

	public void levelUpWaterMimi(){
		GameObject.Find("Manager").GetComponent<EarnManager>().canUpWaterMimi();
		StartCoroutine("print");
	}

	IEnumerator print(){
		yield return new WaitForSeconds(.1f);

		SceneManager.LoadScene(1);

		GameObject.Find("Manager").GetComponent<SaveManager>().Save();
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InventoryManager : MonoBehaviour {

	private int screws;
	public Text screwsText;

	private int nuts;
	public Text nutsText;

	void Start(){
		screws = GameObject.Find("Manager").GetComponent<EarnManager>().screws;
		nuts = GameObject.Find("Manager").GetComponent<EarnManager>().nuts;

		printEarnNuts();
		printEarnScrews();
	}

	void printEarnScrews(){
		string screwStr = screws.ToString("0000");

		screwsText.text = screwStr;
	}

	void printEarnNuts(){
		string nutsStr = nuts.ToString("0000");

		nutsText.text = nutsStr;
	}

	public void backMenu(){
		DontDestroyOnLoad(GameObject.Find("Manager"));
		SceneManager.LoadScene(0);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GemManager : MonoBehaviour {

	public GameObject gemTile;
	public Sprite[] TilePrefabs;

	//public GameObject selector;

	public bool isSelected = false;
	public bool isMatched = false;

	public int gemId;

	public bool MaxYellowAmount = false;
	public bool MaxRedAmount = false;
	public bool MaxGreenAmount = false;
	public bool MaxBlueAmount = false;
	public bool MaxHeartAmount = false;

	public bool gemIni = true;

	public GameObject[] gems;

	public bool firstHit = false;

	public int pA; 

	private LineRenderer line;

	public int XCoord{
		get{
			return Mathf.RoundToInt(transform.localPosition.x);
		}
	}

	public int YCoord{
		get{
			return Mathf.RoundToInt(transform.localPosition.y);
		}
	}

	void Awake(){
		createGemId();

		CreateGem();

		pA = GameObject.Find("Board").GetComponent<BoardManager>().pA;
	}

	void Start(){
		line = GetComponent<LineRenderer>();
	}

//	void Update(){
//		gems = GameObject.FindGameObjectsWithTag("Gem");
//	}
		
	public void ToggleSelector(){
		isSelected = !isSelected;
		//selector.SetActive (isSelected);
	}

	public void Deselect(){
		isSelected = false;
		//selector.SetActive (isSelected);
	}

	public void CreateGem(){
		if(gemIni){
			int randomTile = Random.Range(0,TilePrefabs.Length);

			if(randomTile == 0){
				GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount ++;
			}

			if(randomTile == 1){
				GameObject.Find("Board").GetComponent<BoardManager>().RedAmount ++;
			}

			if(randomTile == 2){
				GameObject.Find("Board").GetComponent<BoardManager>().GreenAmount ++;
			}

			if(randomTile == 3){
				GameObject.Find("Board").GetComponent<BoardManager>().BlueAmount ++;
			}

			if(randomTile == 4){
				GameObject.Find("Board").GetComponent<BoardManager>().HeartAmount ++;
			}

			if(GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount >= 7){
				randomTile = Random.Range(1,TilePrefabs.Length);
			}

			if(GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount >= 7 && 
				GameObject.Find("Board").GetComponent<BoardManager>().RedAmount >= 7){
				randomTile = Random.Range(2,TilePrefabs.Length);
			}
			if(GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount >= 7 && 
				GameObject.Find("Board").GetComponent<BoardManager>().RedAmount >= 7 &&
				GameObject.Find("Board").GetComponent<BoardManager>().GreenAmount >= 7){
				randomTile = Random.Range(3,TilePrefabs.Length);
			}
			if(GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount >= 7 && 
				GameObject.Find("Board").GetComponent<BoardManager>().RedAmount >= 7 &&
				GameObject.Find("Board").GetComponent<BoardManager>().GreenAmount >= 7 &&
				GameObject.Find("Board").GetComponent<BoardManager>().BlueAmount >= 7){
				randomTile = 4;
			}

			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];

			createGemId();

			isMatched = false;
		}

		else if(!gemIni){
			int randomTile = Random.Range(0,TilePrefabs.Length);

			if(randomTile == 0){
				GameObject.Find("Board").GetComponent<BoardManager>().YellowAmount ++;
			}

			if(randomTile == 1){
				GameObject.Find("Board").GetComponent<BoardManager>().RedAmount ++;
			}

			if(randomTile == 2){
				GameObject.Find("Board").GetComponent<BoardManager>().GreenAmount ++;
			}

			if(randomTile == 3){
				GameObject.Find("Board").GetComponent<BoardManager>().BlueAmount ++;
			}

			if(randomTile == 4){
				GameObject.Find("Board").GetComponent<BoardManager>().HeartAmount ++;
			}

			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];

			createGemId();

			isMatched = false;
		}

	}

	private void createGemId(){
		
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0])){
			gemId = 1;//Yellow gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1])){
			gemId = 2;//red gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2])){
			gemId = 3;//green gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3])){
			gemId = 4;//blue gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemId = 5;//life gems
		}
	}


	public void destroyYellow(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0])){
			int randomTile = Random.Range(1,TilePrefabs.Length);
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];
		}

		createGemId();
	}

	public void destroyRed(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1])){
			int randomTile = Random.Range(0,TilePrefabs.Length);
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];
		}

		createGemId();
	}

	public void destroyGreen(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2])){
			int randomTile = Random.Range(0,TilePrefabs.Length);
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];
		}

		createGemId();
	}

	public void destroyBlue(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3])){
			int randomTile = Random.Range(0,TilePrefabs.Length);
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];
		}

		createGemId();
	}

	void OnMouseOver() {
		
		if(Input.GetMouseButton(0)) {
			Collider2D hit = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));
			if(hit != null && !isSelected){
				if(!firstHit){
					ToggleSelector();
					GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Add(hit.gameObject.GetComponent<GemManager>());
					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().firstHit = true;
					}
				}

				if(GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[0].GetComponent<GemManager>().gemId == hit.gameObject.GetComponent<GemManager>().gemId){
					ToggleSelector();
					GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Add(hit.gameObject.GetComponent<GemManager>());
				}

				if(GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[0].GetComponent<GemManager>().gemId != hit.gameObject.GetComponent<GemManager>().gemId && GameObject.Find("Board").GetComponent<BoardManager>().pA > 0){
					ToggleSelector();
					GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Add(hit.gameObject.GetComponent<GemManager>());

					GameObject.Find("Board").GetComponent<BoardManager>().pA -= 2;
					GameObject.Find("Board").GetComponent<BoardManager>().printSustractPAs();
					GameObject.Find("GameManager").GetComponent<MazController>().pAAccumulated += 2;
				}

				if(GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[0].GetComponent<GemManager>().gemId != hit.gameObject.GetComponent<GemManager>().gemId && GameObject.Find("Board").GetComponent<BoardManager>().pA <= 0){

					for(int i = 0; i < gems.Length; i++){
						gems[i].GetComponent<GemManager>().Deselect();

						gems[i].GetComponent<GemManager>().line.enabled = false;

						gems[i].GetComponent<GemManager>().line.SetPosition(0, Vector3.zero);
						gems[i].GetComponent<GemManager>().line.SetPosition(1, Vector3.zero);

						GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Clear();
						gems[i].GetComponent<GemManager>().firstHit = false;
					}
				}

				for(int i = 0; i < GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Count; i++){
					GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].GetComponent<GemManager>().line.enabled = true;
					Vector3 adding = new Vector3(0,0,-2);

					GemManager thisGem = GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i];
					GemManager nextGem = GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i + 1];

					if(thisGem){
						GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].GetComponent<GemManager>().line.SetPosition(0, GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].transform.position + adding);
						GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].GetComponent<GemManager>().line.SetPosition(1, GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].transform.position + adding);
					}
					
					if(nextGem){
						GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].GetComponent<GemManager>().line.SetPosition(0, GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].transform.position + adding);
						GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i].GetComponent<GemManager>().line.SetPosition(1, GameObject.Find("Board").GetComponent<BoardManager>().Neighbors[i + 1].transform.position + adding);
					}
				}
			}
		}
//
//		if(Input.GetMouseButtonUp(0)) {
//			GameObject.Find("Board").GetComponent<BoardManager>().CheckForNearbyMatches();
//
//			for(int i = 0; i < gems.Length; i++){
//				gems[i].GetComponent<GemManager>().Deselect();
//
//				gems[i].GetComponent<GemManager>().line.enabled = false;
//
//				gems[i].GetComponent<GemManager>().line.SetPosition(0, Vector3.zero);
//				gems[i].GetComponent<GemManager>().line.SetPosition(1, Vector3.zero);
//
//				GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Clear();
//				gems[i].GetComponent<GemManager>().firstHit = false;
//			}
//		}
	}

	void Update(){
		gems = GameObject.FindGameObjectsWithTag("Gem");

		if(Input.GetMouseButtonUp(0)) {
			GameObject.Find("Board").GetComponent<BoardManager>().CheckForNearbyMatches();

			for(int i = 0; i < gems.Length; i++){
				gems[i].GetComponent<GemManager>().Deselect();

				gems[i].GetComponent<GemManager>().line.enabled = false;

				gems[i].GetComponent<GemManager>().line.SetPosition(0, Vector3.zero);
				gems[i].GetComponent<GemManager>().line.SetPosition(1, Vector3.zero);

				GameObject.Find("Board").GetComponent<BoardManager>().Neighbors.Clear();
				gems[i].GetComponent<GemManager>().firstHit = false;
			}
		}
	}
}

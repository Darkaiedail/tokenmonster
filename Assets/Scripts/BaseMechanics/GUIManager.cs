﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

	public Text scoreRedText;
	public Text scoreBlueText;
	public Text scoreYellowText;
	public Text scoreGreenText;
	public Text scoreHeartText;
	private int scoreRed = 0;
	private int scoreBlue = 0;
	private int scoreYellow = 0;
	private int scoreGreen = 0;
	private int scoreHeart = 0;


	void Update(){
		printScoreRed();
		printScoreBlue();
		printScoreYellow();
		printScoreGreen();
		printScoreLife();
	}

	public void AddScore(int AmountYellowToken, int AmountRedToken, int AmountGreenToken, int AmountBlueToken, int AmountHeartToken){
		scoreRed += AmountRedToken;
		scoreBlue += AmountBlueToken;
		scoreYellow += AmountYellowToken;
		scoreGreen += AmountGreenToken;
		scoreHeart += AmountHeartToken;
	}

	void printScoreRed(){
		string scoreStr = scoreRed.ToString("00");

		scoreRedText.text = scoreStr;
	}

	void printScoreBlue(){
		string scoreStr = scoreBlue.ToString("00");

		scoreBlueText.text = scoreStr;
	}

	void printScoreYellow(){
		string scoreStr = scoreYellow.ToString("00");

		scoreYellowText.text = scoreStr;
	}

	void printScoreGreen(){
		string scoreStr = scoreGreen.ToString("00");

		scoreGreenText.text = scoreStr;
	}

	void printScoreLife(){
		string scoreStr = scoreHeart.ToString("00");

		scoreHeartText.text = scoreStr;
	}
}

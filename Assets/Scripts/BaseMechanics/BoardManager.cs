﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour {

	public List<GemManager> gems = new List<GemManager>();
	public List<GemManager> Neighbors = new List<GemManager>();
	public int GridWidth;
	public int GridHeight;
	public float gemSpace = 1.25f;
	public GameObject gemPrefab;

	public int AmountToMatch = 3;
	public bool isMatched = false;

	public int YellowAmount = 0;
	public int RedAmount = 0;
	public int GreenAmount = 0;
	public int BlueAmount = 0;
	public int HeartAmount = 0;
	private bool nextGem = false;

	private GameObject[] enemies;
	private GameObject[] characters;

	public int randomEnemy;
	public GameObject[] enemyArray;
	public bool firstRound = false;

	public int pA = 16;
	public int sustarctPA = - 2;
	public Text pAsText;
	public Text sustractPAsText;

	void Start(){
		if(!nextGem){
			for(int i = 0; i < GridWidth; i++){
				for(int j = 0; j < GridHeight; j++){
					GameObject g = Instantiate(gemPrefab, new Vector2 (i/gemSpace,j/gemSpace), Quaternion.identity) as GameObject;
					g.transform.parent = gameObject.transform;
					gems.Add(g.GetComponent<GemManager>());
				}
			}

			gameObject.transform.position = new Vector2(-1.44f,-1.13f);
		}

		else{
			for(int i = 0; i < GridWidth; i++){
				for(int j = 0; j < GridHeight; j++){
					GameObject g = Instantiate(gemPrefab, new Vector2 (i/gemSpace,j/gemSpace), Quaternion.identity) as GameObject;
					g.transform.parent = gameObject.transform;
					gems.Add(g.GetComponent<GemManager>());

					gems[i].GetComponent<GemManager>().gemIni = false;
				}
			}
		}
	}

	void Update(){
		
		printPAs();

		if(firstRound){
			enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
			randomEnemy = Random.Range(0,enemyArray.Length);
			print(randomEnemy);
			firstRound = false;
		}

		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		characters = GameObject.FindGameObjectsWithTag("Player");

		if(isMatched){
			for(int i = 0; i < gems.Count; i++){
				if(gems[i].isMatched){
					gems[i].CreateGem();

					if(Resources.Equals(gems[0].GetComponent<SpriteRenderer>().sprite, gems[1].GetComponent<SpriteRenderer>().sprite)){
						gems[i].GetComponent<GemManager>().CreateGem();
					}

					gems[i].transform.position = new Vector3(gems[i].transform.position.x,
						gems[i].transform.position.y + 6, gems[i].transform.position.z);

					gems[i].GetComponent<GemManager>().gemIni = false;
					nextGem = true;
				}
			}

			isMatched = false;

			for(int i = 0; i < enemies.Length; i++){
				enemies[i].GetComponent<EnemyController>().subtractTurns();
			}
		}
	}


	public void CheckForNearbyMatches(){

		FixMatchList(Neighbors);
	}

	public void FixMatchList(List<GemManager> ListToFix){

		if(ListToFix.Count >= AmountToMatch){

			foreach(GemManager gem in ListToFix){
				if(gem.gemId == 1){

					GameObject.Find("Canvas").GetComponent<GUIManager>().AddScore((ListToFix.Count - 2)/3,0,0,0,0);

					for(int i = 0; i < characters.Length; i++){
						characters[i].GetComponent<CharController>().numberYellowTokensDestroy((ListToFix.Count - 2)/3);
						characters[i].GetComponent<HabilityController>().numberYellowTokensToHability((ListToFix.Count - 2)/3);
					}

					print("yellow");
				}
				if(gem.gemId == 2){

					GameObject.Find("Canvas").GetComponent<GUIManager>().AddScore(0,(ListToFix.Count - 2)/3,0,0,0);

					for(int i = 0; i < characters.Length; i++){
						characters[i].GetComponent<CharController>().numberRedTokensDestroy((ListToFix.Count - 2)/3);
						characters[i].GetComponent<HabilityController>().numberRedTokensToHability((ListToFix.Count - 2)/3);
					}

					print("red");
				}
				if(gem.gemId == 3){

					GameObject.Find("Canvas").GetComponent<GUIManager>().AddScore(0,0,(ListToFix.Count - 2)/3,0,0);

					for(int i = 0; i < characters.Length; i++){
						characters[i].GetComponent<CharController>().numberGreenTokensDestroy((ListToFix.Count - 2)/3);
						characters[i].GetComponent<HabilityController>().numberGreenTokensToHability((ListToFix.Count - 2)/3);
					}

					print("green");
				}
				if(gem.gemId == 4){

					GameObject.Find("Canvas").GetComponent<GUIManager>().AddScore(0,0,0,(ListToFix.Count - 2)/3,0);

					for(int i = 0; i < characters.Length; i++){
						characters[i].GetComponent<CharController>().numberBlueTokensDestroy((ListToFix.Count - 2)/3);
						characters[i].GetComponent<HabilityController>().numberBlueTokensToHability((ListToFix.Count - 2)/3);
					}

					print("blue");
				}
				if(gem.gemId == 5){

					GameObject.Find("Canvas").GetComponent<GUIManager>().AddScore(0,0,0,0,(ListToFix.Count - 2)/3);

					for(int i = 0; i < characters.Length; i++){
						characters[i].GetComponent<CharController>().cure((ListToFix.Count - 2)/3);
					}

					print("pink");
				}
			}

			isMatched = true;

			for(int i = 0; i < ListToFix.Count; i++){
				ListToFix[i].isMatched = true;
			}
		}
	}

	void printPAs(){
		string pAStr = pA.ToString("00");

		pAsText.text = pAStr;
	}

	public void printSustractPAs(){
		sustractPAsText.gameObject.SetActive(true);

		string pAStr = sustarctPA.ToString("00");

		sustractPAsText.text = pAStr;

		iTween.MoveAdd(sustractPAsText.gameObject, iTween.Hash("x", .2f,
			"time", .3f, "easetype","easeOutBounce", "looptype", "pingPong"));
		
		StartCoroutine("desactiveSustractPAsText");
	}

	IEnumerator desactiveSustractPAsText (){
		yield return new WaitForSeconds(.6f);
		sustractPAsText.gameObject.SetActive(false);
	}
}

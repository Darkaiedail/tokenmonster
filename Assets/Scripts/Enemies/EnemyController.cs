﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	public float life = 1400f;
	private float health; 

	public enum typeOfEnemy {Ray, Fire, Plant, Water};
	public typeOfEnemy type;

	public float enemyDamage;
	public int numTurnsToAttack;

	private int restOfTurnsToAttack;
	private bool attack;

	public GameObject[] characters;

	public GameObject selector;

	public Text turnText;
	public Scrollbar healthBar;

	public int randomEnemy;
	public GameObject[] enemyArray;

	public int numEnem;
	public bool LastRound;

	void Start(){
		restOfTurnsToAttack = numTurnsToAttack;
		characters = GameObject.FindGameObjectsWithTag("Player");

		health = life;

		turnText.transform.parent = GameObject.Find("Canvas").transform;
		healthBar.transform.parent = GameObject.Find("Canvas").transform;

		enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
		numEnem = enemyArray.Length;
	}

	public void subtractTurns(){
		restOfTurnsToAttack -= 1;
	}

	public void findCharacters(){
		characters = GameObject.FindGameObjectsWithTag("Player");
	}

	void Update(){
		printTurns();
	
		//configuracion del rayo
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		//lo que trae el rayo de vuelta
		RaycastHit hit = new RaycastHit ();

		if (Physics.Raycast (ray, out hit, 1000)) {

			if(Input.GetMouseButtonDown(0) && hit.collider.gameObject.CompareTag ("Enemy")){//1 bot dcho, 0 bot izdo, 3 bot centro
				for(int i = 0; i < characters.Length; i++){
					characters[i].GetComponent<CharController>().enemySelected(hit.collider.gameObject);
					characters[i].GetComponent<HabilityController>().enemySelected(hit.collider.gameObject);
				}
				selector.SetActive(false);
				hit.collider.gameObject.GetComponent<EnemyController>().selector.SetActive(true);
			}
		}

		if(restOfTurnsToAttack == 0){
			attack = true;
			restOfTurnsToAttack = numTurnsToAttack;
		}

		if(attack){
			if(type == typeOfEnemy.Ray){
				if(characters != null){
					for(int i = 0; i < characters.Length; i++){
						if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Ray){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Fire){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Plant){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.3f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 0.6f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 0.7f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 0.5f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Water){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 1.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 2.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 2.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 2.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
					}
				}
			}
			else if(type == typeOfEnemy.Fire){
				if(characters != null){
					for(int i = 0; i < characters.Length; i++){
						if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Ray){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Fire){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Plant){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 1.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 2.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 2.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 2.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Water){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.3f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 0.6f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 0.7f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 0.5f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
					}
				}
			}
			else if(type == typeOfEnemy.Plant){
				if(characters != null){
					for(int i = 0; i < characters.Length; i++){
						if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Ray){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 1.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 2.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 2.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 2.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Fire){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.3f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 0.6f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 0.7f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 0.5f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Plant){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Water){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
					}
				}
			}
			else if(type == typeOfEnemy.Water){
				if(characters != null){
					for(int i = 0; i < characters.Length; i++){
						if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Ray){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.3f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 0.6f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 0.7f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 0.5f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Fire){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 1.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 2.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 2.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 2.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Plant){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
						else if(characters[i].GetComponent<CharController>().charType == CharController.TypeOfChar.Water){
							if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Tank){
								float baseDamage = 0.8f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Special){
								float baseDamage = 1.1f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Damage){
								float baseDamage = 1.2f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
							else if(characters[i].GetComponent<CharController>().charClass == CharController.ClassOfChar.Balance){
								float baseDamage = 1.0f;

								float totalDamage = enemyDamage * baseDamage;
								characters[i].GetComponent<CharController>().defeat(totalDamage);
								characters[i].GetComponent<CharController>().printCharDamage(totalDamage);
							}
						}
					}
				}
			}

			attack = false;
		}
			
		if(health <= 0){
			die();
		}

		enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
	}

	public void kill(float damage){
		health -= damage;

		float healthSize = health/life;
		healthBar.size = healthSize;
	}

	void die(){
		Destroy(this.gameObject);
		turnText.gameObject.SetActive(false);
		healthBar.gameObject.SetActive(false);

		characters = GameObject.FindGameObjectsWithTag("Player");

		for(int i = 0; i < characters.Length; i++){
			characters[i].GetComponent<CharController>().selectRandomEnemy();
		}

		for(int i = 0; i < enemyArray.Length; i++){
			enemyArray[i].GetComponent<EnemyController>().numEnem -= 1;
		}

		if(numEnem == 0){
			GameObject.Find("GameManager").GetComponent<MazController>().nextRoundTrue();
			for(int i = 0; i < characters.Length; i++){
				characters[i].GetComponent<CharController>().selectRandomEnemy();
			}
		}
	}

	void printTurns(){
		string turnsStr = restOfTurnsToAttack.ToString("00");

		turnText.text = turnsStr;
	}
}

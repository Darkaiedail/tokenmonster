﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MazController : MonoBehaviour {

	public GameObject[] characters;
	public GameObject[] charPos;

	public int numOfRounds;
	public int[] numEnemRound;

	public GameObject[] enemies;
	public GameObject[] bosses;
	public GameObject[] positions;

	public GameObject[] roundsText;
	public GameObject bossText;

	[HideInInspector]
	public GameObject[] sceneCharacters;
	[HideInInspector]
	public GameObject[] sceneEnemies;

	private bool nextRound;

	public int earnExp;
	public int earnGold;
	public int earnChest;

	public int exp;
	public int gold;
	public int chest;
	public Text coinText;
	public Text expText;
	public GameObject chestPanel;

	public GameObject panelFinal;

	private int pA;
	[HideInInspector]
	public int pAAccumulated;
	public int maxPA;

	protected string currentLevel;
	protected int worldIndex;
	protected int levelIndex;

	public int stars;
	public GameObject starsPanel;
	public GameObject firstStar;
	public GameObject secondStar;
	public GameObject thirdStar;

	void Awake(){
		GameObject manager = GameObject.Find("ManagerSelection");

		if(manager.GetComponent<ManagerSelection>().i != 4){
			GameObject charOne = Instantiate(characters[manager.GetComponent<ManagerSelection>().i], charPos[0].transform.position, Quaternion.identity) as GameObject;

			charOne.GetComponent<HabilityController>().h = 0;
			charOne.GetComponent<CharController>().h = 0;
		}
		if(manager.GetComponent<ManagerSelection>().j != 4){
			GameObject charTwo = Instantiate(characters[manager.GetComponent<ManagerSelection>().j], charPos[1].transform.position, Quaternion.identity) as GameObject;

			charTwo.GetComponent<HabilityController>().h = 1;
			charTwo.GetComponent<CharController>().h = 1;
		}
		if(manager.GetComponent<ManagerSelection>().k != 4){
			GameObject charThree = Instantiate(characters[manager.GetComponent<ManagerSelection>().k], charPos[2].transform.position, Quaternion.identity) as GameObject;

			charThree.GetComponent<HabilityController>().h = 2;
			charThree.GetComponent<CharController>().h = 2;
		}
		if(manager.GetComponent<ManagerSelection>().h != 4){
			GameObject charFour = Instantiate(characters[manager.GetComponent<ManagerSelection>().h], charPos[3].transform.position, Quaternion.identity) as GameObject;

			charFour.GetComponent<HabilityController>().h = 3;
			charFour.GetComponent<CharController>().h = 3;
		}
	}

	void Start(){
		pA = GameObject.Find("Board").GetComponent<BoardManager>().pA;

		roundsText[numOfRounds].SetActive(true);
		StartCoroutine("desactiveText");

		sceneCharacters = GameObject.FindGameObjectsWithTag("Player");
		for(int i = 0; i < numEnemRound[0]; i++){
			Instantiate(enemies[Random.Range(0,enemies.Length)], positions[i].transform.position, Quaternion.identity);
		}
		for(int i = 0; i < sceneCharacters.Length; i++){
			sceneCharacters[i].GetComponent<CharController>().firstRound = true;
		}

		sceneEnemies = GameObject.FindGameObjectsWithTag("Enemy");
		GameObject.Find("Board").GetComponent<BoardManager>().firstRound = true;

		numOfRounds -= 1;

		currentLevel = Application.loadedLevelName;
	}

	void Update () {
		if(nextRound){
			nextRound = false;

			numOfRounds -= 1;

			if(numOfRounds > 0){
				roundsText[numOfRounds].SetActive(true);
				StartCoroutine("desactiveText");

				for(int j = 0; j < numEnemRound.Length - 1; j++){
					for(int i = 0; i < numEnemRound[j + 1]; i++){
						Instantiate(enemies[Random.Range(0,enemies.Length)], positions[i].transform.position, Quaternion.identity);
					}
				}

				GameObject.Find("Board").GetComponent<BoardManager>().pA = pA - 2;

				sceneEnemies = GameObject.FindGameObjectsWithTag("Enemy");
			}

			else if(numOfRounds == 0){
				bossText.SetActive(true);
				StartCoroutine("desactiveText");

				Instantiate(bosses[Random.Range(0,bosses.Length)], new Vector3 (.82f, 5.07f, -1.5f), Quaternion.identity);

				GameObject.Find("Board").GetComponent<BoardManager>().pA = pA - 2;
			}

			else if(numOfRounds < 0){
				for(int i = 0; i < sceneCharacters.Length; i++){
					sceneCharacters[i].GetComponent<CharController>().mazEnd = true;

					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.RayMimi){
						GameObject.Find("Manager").GetComponent<EarnManager>().expRayMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.FireMimi){
						GameObject.Find("Manager").GetComponent<EarnManager>().expFireMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.PlantMimi){
						GameObject.Find("Manager").GetComponent<EarnManager>().expPlantMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.WaterMimi){
						GameObject.Find("Manager").GetComponent<EarnManager>().expWaterMimi += earnExp;
					}

					if(sceneCharacters[i].GetComponent<CharController>().health >= 0){
						stars += 1;
					}
				}

				if(pAAccumulated <= maxPA){
					stars += 1;
				}

				exp += earnExp;
				gold += earnGold;
				chest += earnChest;
				stars += 1;

				GameObject.Find("Manager").GetComponent<EarnManager>().coins += earnGold;

				panelFinal.SetActive(true);
				printCoins();
				printExp();
				print("You Win");
			}
		}
	}

	public void nextRoundTrue(){
		nextRound = true;
		print("caca");
	}

	IEnumerator desactiveText(){
		yield return new WaitForSeconds(0.5f);

		for(int i = 0; i < roundsText.Length; i++){
			roundsText[i].SetActive(false);
		}

		bossText.SetActive(false);
	}

	void printCoins(){
		string coinsStr = gold.ToString("000");

		coinText.text = coinsStr;
	}

	void printExp(){
		string coinsStr = exp.ToString("000");

		expText.text = coinsStr;
	}

	public void winStars(){
		panelFinal.SetActive(false);
		starsPanel.SetActive(true);

		if(stars == 1){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));
		}

		if(stars == 2){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));

			StartCoroutine("activateSecondStar");
		}

		if(stars == 6){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));
			
			StartCoroutine("activateSecondStar");
			
			StartCoroutine("activateThirdStar");
		}
	}

	IEnumerator activateSecondStar(){
		yield return new WaitForSeconds(.5f);

		secondStar.SetActive(true);
		iTween.ScaleTo(secondStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
			"time", .5f, "easetype","easeOutQuint"));
	}

	IEnumerator activateThirdStar(){
		yield return new WaitForSeconds(1.0f);

		thirdStar.SetActive(true);
		iTween.ScaleTo(thirdStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
			"time", .5f, "easetype","easeOutQuint"));
	}

	public void backMenu(){
		starsPanel.SetActive(false);

		if(stars >= 6){
			if(chest > 0){
				chestPanel.SetActive(true);
			}

			if(chest == 0){
				SceneManager.LoadScene(0);

				//GameObject.Find("Manager").GetComponent<EarnManager>().nextLevel = 0;
				//GameObject.Find("Manager").GetComponent<EarnManager>().nextLevel = GameObject.Find("Manager").GetComponent<EarnManager>().sceneLevel + 1 - 3;

				UnlockLevels();

				GameObject.Find("Manager").GetComponent<SaveManager>().Save();
			}
		}

		else {
			SceneManager.LoadScene(0);

			UnlockLevels();

			GameObject.Find("Manager").GetComponent<SaveManager>().Save();
		}
	}

	public void recieveChest(){
		if(chest > 0){
			GameObject.Find("GameManager").GetComponent<ChestController>().recieveReward();
			chest -= 1;
		}
	}

	protected void  UnlockLevels (){
		//set the playerprefs value of next level to 1 to unlock
		for(int i = 0; i < LockLevel.worlds; i++){
			for(int j = 1; j < LockLevel.levels; j++){               
				if(currentLevel == "Level" + (i+1).ToString() + "." + j.ToString()){
					worldIndex  = (i + 1);
					levelIndex  = (j + 1);
					PlayerPrefs.SetInt("level" + worldIndex.ToString() + ":" + levelIndex.ToString(),1);
				}
			}
		}
		//load the World1 level 
		SceneManager.LoadScene("Menu");
	}
}
